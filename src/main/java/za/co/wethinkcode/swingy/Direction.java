package za.co.wethinkcode.swingy;

public enum Direction {
    NORTH, SOUTH, EAST, WEST
}
