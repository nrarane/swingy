package za.co.wethinkcode.swingy.controller;

import za.co.wethinkcode.swingy.view.Arena;

public class ConsoleController implements GameController {

    public Arena arena;

    public ConsoleController(Arena arena) {
        this.arena = arena;
    }

    public void moveSouth() {

    }

    public void moveNorth() {

    }

    public void moveEast() {

    }

    public void moveWest() {

    }

    public void controlPlayer(String heroName) {

    }

    public void attack() {

    }

    public void run() {

    }

    public void initializeComponents() {

    }

}
